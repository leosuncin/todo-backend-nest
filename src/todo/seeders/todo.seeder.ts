import type { DataSource } from 'typeorm';
import type { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Todo } from '~todo/entities/todo.entity';

export class TodoSeeder implements Seeder {
  async run(
    _: DataSource,
    factoryManager: SeederFactoryManager,
  ): Promise<void> {
    const todoFactory = factoryManager.get(Todo);
    await todoFactory.save({
      id: 'a6119594-5067-4336-afbf-7efb2a38aac0',
      title: 'Make a sandwich',
      completed: false,
    });
    await todoFactory.saveMany(9);
  }
}
