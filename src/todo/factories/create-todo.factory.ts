import { faker } from '@faker-js/faker';
import { FactoryBuilder } from 'factory.io';

import { CreateTodo } from '~todo/dto/create.todo.dto';

export const createTodoFactory = FactoryBuilder.of(CreateTodo)
  .options({ removeUnassignedProperties: true })
  .props({
    title: faker.lorem.sentence,
  })
  .build();
