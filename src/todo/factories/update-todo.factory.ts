import { faker } from '@faker-js/faker';
import { FactoryBuilder } from 'factory.io';

import { UpdateTodo } from '~todo/dto/update-todo.dto';
import { createTodoFactory } from '~todo/factories/create-todo.factory';

export const updateTodoFactory = FactoryBuilder.of(UpdateTodo)
  .options({
    removeUnassignedProperties: true,
  })
  .mixins([createTodoFactory])
  .prop('completed', faker.datatype.boolean)
  .build();
