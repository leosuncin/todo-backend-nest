import { setSeederFactory } from 'typeorm-extension';

import { Todo } from '~todo/entities/todo.entity';

export const todoFactory = setSeederFactory(Todo, (faker) =>
  Todo.fromPartial({
    completed: faker.datatype.boolean(),
    title: faker.hacker.phrase(),
    createdAt: faker.date.recent(),
  }),
);
