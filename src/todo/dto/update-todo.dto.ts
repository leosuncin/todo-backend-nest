import { PartialType } from '@nestjs/mapped-types';
import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional } from 'class-validator';

import { CreateTodo } from '~todo/dto/create.todo.dto';

export class UpdateTodo extends PartialType(CreateTodo) {
  /**
   * Whether the task is completed
   */
  @IsOptional()
  @IsBoolean()
  @Transform(({ value }) =>
    typeof value === 'string'
      ? ['true', '1', 'yes'].includes(value.toLocaleLowerCase())
      : value,
  )
  readonly completed?: boolean;
}
