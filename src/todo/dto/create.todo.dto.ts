import {
  IsDefined,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
} from 'class-validator';

export class CreateTodo {
  /**
   * The title of the task
   */
  @IsDefined()
  @IsString()
  @IsNotEmpty()
  readonly title!: string;

  /**
   * The order of the task
   */
  @IsOptional()
  @IsInt()
  @IsPositive()
  readonly order?: number;
}
