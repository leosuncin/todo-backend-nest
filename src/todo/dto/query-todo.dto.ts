import { IsOptional } from 'class-validator';

import { Todo } from '~todo/entities/todo.entity';

export class QueryTodo {
  @IsOptional()
  fields?:
    | `${'+' | '-' | ''}${keyof Todo}`
    | Array<`${'+' | '-' | ''}${keyof Todo}`>;

  @IsOptional()
  filter?: Record<keyof Todo, string | Array<string>>;

  @IsOptional()
  page?: Partial<{
    limit: number;
    offset: number;
  }>;

  @IsOptional()
  sort?:
    | `${'+' | '-' | ''}${keyof Todo}`
    | Array<`${'+' | '-' | ''}${keyof Todo}`>
    | Record<keyof Todo, 'DESC' | 'ASC'>;
}
