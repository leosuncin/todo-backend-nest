import { faker } from '@faker-js/faker';
import { CallHandler } from '@nestjs/common';
import { ExecutionContextHost } from '@nestjs/core/helpers/execution-context-host';
import { createMocks } from 'node-mocks-http';
import { lastValueFrom, of } from 'rxjs';

import { Todo } from '~todo/entities/todo.entity';
import {
  AddUrlInterceptor,
  type TodoWithUrl,
} from '~todo/interceptors/add-url.interceptor';
import { todoFactory } from '~todo/factories/todo.factory';

describe('AddUrlInterceptor', () => {
  it('should be defined', () => {
    expect(new AddUrlInterceptor()).toBeDefined();
  });

  it('should add the url to one todo', async () => {
    const { req, res } = createMocks({
      method: 'PATCH',
      path: '/todos/a6119594-5067-4336-afbf-7efb2a38aac0',
      params: {
        id: 'a6119594-5067-4336-afbf-7efb2a38aac0',
      },
      headers: {
        host: 'localhost',
      },
      protocol: 'http',
      hostname: 'localhost',
    });
    const context = new ExecutionContextHost([req, res]);
    const next: CallHandler<Todo> = {
      handle: () =>
        of(Todo.fromPartial({ id: 'a6119594-5067-4336-afbf-7efb2a38aac0' })),
    };
    const interceptor = new AddUrlInterceptor();

    await expect(
      lastValueFrom(interceptor.intercept(context, next)),
    ).resolves.toHaveProperty(
      'url',
      'http://localhost/todos/a6119594-5067-4336-afbf-7efb2a38aac0',
    );
  });

  it('should add the url to each todo', async () => {
    const { req, res } = createMocks({
      path: '/todos',
      headers: {
        host: 'www.example.com',
        'x-forwarded-for': '100.0.10.101',
        'x-forwarded-proto': 'https',
      },
      protocol: 'http',
      hostname: 'localhost',
    });
    const context = new ExecutionContextHost([req, res]);
    const next: CallHandler<Array<Todo>> = {
      handle: () =>
        of(
          Array.from(
            { length: 5 },
            () => todoFactory.factoryFn(faker, undefined) as Todo,
          ),
        ),
    };
    const interceptor = new AddUrlInterceptor();
    const todos = (await lastValueFrom(
      interceptor.intercept(context, next),
    )) as TodoWithUrl[];

    expect(todos.every((todo) => typeof todo.url === 'string')).toBe(true);
  });
});
