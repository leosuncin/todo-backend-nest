import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import type { Request } from 'express';
import type { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Todo } from '~todo/entities/todo.entity';

export type TodoWithUrl = Todo & { url: string };

const buildSetUrl = (request: Request) => {
  const protocol = request.header('x-forwarded-proto') ?? request.protocol;
  const host = request.header('host');

  return (todo: Todo): TodoWithUrl =>
    Object.assign(todo, {
      url: `${protocol}://${host}/todos/${todo.id}`,
    });
};

@Injectable()
export class AddUrlInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<Todo | Array<Todo>>,
  ): Observable<TodoWithUrl | Array<TodoWithUrl>> {
    const request = context.switchToHttp().getRequest<Request>();
    const setUrl = buildSetUrl(request);

    return next
      .handle()
      .pipe(
        map((todo) =>
          Array.isArray(todo) ? todo.map((todo) => setUrl(todo)) : setUrl(todo),
        ),
      );
  }
}
