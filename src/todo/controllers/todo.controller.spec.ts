import { Test, TestingModule } from '@nestjs/testing';

import { TodoController } from '~todo/controllers/todo.controller';
import { TodoService } from '~todo/services/todo.service';
import { CreateTodo } from '~todo/dto/create.todo.dto';
import { Todo } from '~todo/entities/todo.entity';
import { It, Mock } from 'moq.ts';

describe('TodoController', () => {
  let controller: TodoController;
  const todo = Todo.fromPartial({
    id: 'a6119594-5067-4336-afbf-7efb2a38aac0',
    title: 'Make a sandwich',
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TodoController],
    })
      .useMocker((token) => {
        if (token === TodoService) {
          const mock = new Mock<TodoService>()
            .setup((service) =>
              service.createOne(It.Is<CreateTodo>((dto) => 'title' in dto)),
            )
            .callback(({ args: [newTodo] }) =>
              Promise.resolve(
                Todo.fromPartial({ completed: false, order: 0, ...newTodo }),
              ),
            )
            .setup((service) => service.findAll(It.IsAny()))
            .returnsAsync([])
            .setup((service) => service.findOne(todo.id))
            .returnsAsync(todo)
            .setup((service) =>
              service.updateOne(
                It.Is((todo) => todo instanceof Todo),
                It.IsAny(),
              ),
            )
            .callback(({ args: [, updates] }) =>
              Promise.resolve(Todo.mergeUpdateTodo(todo, updates)),
            )
            .setup((service) =>
              service.removeOne(It.Is((todo) => todo instanceof Todo)),
            )
            .returnsAsync(todo)
            .setup((service) => service.removeAll())
            .returnsAsync([]);

          return mock.object();
        }
      })
      .compile();

    controller = module.get<TodoController>(TodoController);
  });

  it('should be an instanceof TodoController', () => {
    expect(controller).toBeInstanceOf(TodoController);
  });

  it('should create a new todo', async () => {
    const newTodo: CreateTodo = {
      title: 'Duis aute irure dolor in reprehenderit in voluptate velit esse',
    };

    const todo = await controller.createOne(newTodo);

    expect(todo).toMatchObject<Todo>({
      id: expect.any(String),
      title: newTodo.title,
      completed: false,
      order: 0,
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
    });
  });

  it('should remove all todos', async () => {
    await expect(controller.removeAll()).resolves.toEqual([]);
  });

  it('should list all todos', async () => {
    const todos = await controller.findAll();

    expect(Array.isArray(todos)).toBe(true);
  });

  it('should get one todo by id', () => {
    expect(controller.findOne(todo)).toStrictEqual(todo);
  });

  it.each([{ completed: true }, { title: 'Lorem ipsum dolor sit amet' }])(
    'should update one todo by id',
    async (updates) => {
      await expect(
        controller.updateOne(todo, updates),
      ).resolves.toMatchObject<Todo>({
        ...todo,
        ...updates,
        updatedAt: expect.any(Date),
      });
    },
  );

  it('should remove one todo by id', async () => {
    await expect(controller.removeOne(todo)).resolves.toBeUndefined();
  });
});
