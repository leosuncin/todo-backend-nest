import {
  Body,
  ClassSerializerInterceptor,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseUUIDPipe,
  Patch,
  Post,
  Query,
  UseInterceptors,
  ValidationPipe,
} from '@nestjs/common';

import { CreateTodo } from '~todo/dto/create.todo.dto';
import { QueryTodo } from '~todo/dto/query-todo.dto';
import { UpdateTodo } from '~todo/dto/update-todo.dto';
import type { Todo } from '~todo/entities/todo.entity';
import { AddUrlInterceptor } from '~todo/interceptors/add-url.interceptor';
import { TodoService } from '~todo/services/todo.service';
import { TodoPipe } from '~todo/pipes/todo.pipe';

@Controller('todos')
@UseInterceptors(ClassSerializerInterceptor)
export class TodoController {
  constructor(private readonly todoService: TodoService) {}

  /**
   * Create a new todo
   */
  @Post()
  @HttpCode(HttpStatus.OK)
  @UseInterceptors(AddUrlInterceptor)
  createOne(
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    createTodo: CreateTodo,
  ) {
    return this.todoService.createOne(createTodo);
  }

  /**
   * Remove all todos
   */
  @Delete()
  @UseInterceptors(AddUrlInterceptor)
  removeAll() {
    return this.todoService.removeAll();
  }

  /**
   * Get the list of todos
   */
  @Get()
  @UseInterceptors(AddUrlInterceptor)
  findAll(@Query() query?: QueryTodo) {
    return this.todoService.findAll(query);
  }

  /**
   * Get one todo by id
   */
  @Get(':id')
  @UseInterceptors(AddUrlInterceptor)
  findOne(@Param('id', ParseUUIDPipe, TodoPipe) todo: Todo) {
    return todo;
  }

  /**
   * Update one todo
   */
  @Patch(':id')
  @UseInterceptors(AddUrlInterceptor)
  updateOne(
    @Param('id', ParseUUIDPipe, TodoPipe) todo: Todo,
    @Body(new ValidationPipe({ transform: true, whitelist: true }))
    updateTodo: UpdateTodo,
  ) {
    return this.todoService.updateOne(todo, updateTodo);
  }

  /**
   * Delete one todo
   */
  @Delete(':id')
  async removeOne(@Param('id', ParseUUIDPipe, TodoPipe) todo: Todo) {
    await this.todoService.removeOne(todo);
  }
}
