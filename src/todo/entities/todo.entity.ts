import { Exclude } from 'class-transformer';
import { randomUUID } from 'node:crypto';
import {
  Column,
  CreateDateColumn,
  DeepPartial,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import type { CreateTodo } from '~todo/dto/create.todo.dto';
import type { UpdateTodo } from '~todo/dto/update-todo.dto';

@Entity()
export class Todo {
  /**
   * The identifier of the task
   */
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  /**
   * The title of the task
   */
  @Column({ type: 'text' })
  title!: string;

  /**
   * Whether the task is completed
   */
  @Column({ type: Boolean, default: false })
  completed!: boolean;

  /**
   * The order of the task
   */
  @Column({ type: 'int', default: 0 })
  order!: number;

  @Exclude()
  @CreateDateColumn()
  readonly createdAt = new Date();

  @Exclude()
  @UpdateDateColumn()
  readonly updatedAt = new Date();

  static fromCreateTodo(dto: CreateTodo): Todo {
    const todo = new Todo();

    todo.title = dto.title;

    return todo;
  }

  static fromPartial(data: DeepPartial<Todo>): Todo {
    return Object.assign(
      new Todo(),
      {
        id: randomUUID(),
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      data,
    );
  }

  static mergeUpdateTodo(todo: Todo, dto: UpdateTodo): Todo {
    const updates: Partial<Todo> = {
      title: dto.title ?? todo.title,
      completed: dto.completed ?? todo.completed,
      updatedAt: new Date(),
    };

    return Object.assign(todo, updates);
  }
}
