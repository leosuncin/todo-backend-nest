import { Test } from '@nestjs/testing';
import { Mock } from 'moq.ts';

import { Todo } from '~todo/entities/todo.entity';
import { TodoPipe } from '~todo/pipes/todo.pipe';
import { TodoService } from '~todo/services/todo.service';

describe('TodoPipe', () => {
  let pipe: TodoPipe;
  const todo = Todo.fromPartial({
    id: 'a6119594-5067-4336-afbf-7efb2a38aac0',
    title: 'Make a sandwich',
  });

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [
        {
          provide: TodoService,
          useFactory() {
            const mock = new Mock<TodoService>()
              .setup((service) => service.findOne(todo.id))
              .returnsAsync(todo);

            return mock.object();
          },
        },
        TodoPipe,
      ],
    }).compile();

    pipe = module.get(TodoPipe);
  });

  it('should be defined', () => {
    expect(pipe).toBeDefined();
  });

  it('should transform the id to a todo', async () => {
    await expect(
      pipe.transform('a6119594-5067-4336-afbf-7efb2a38aac0'),
    ).resolves.toBeInstanceOf(Todo);
  });
});
