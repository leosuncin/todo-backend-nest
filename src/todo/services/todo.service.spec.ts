import { NotFoundException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { randomUUID } from 'node:crypto';
import { Mock, It } from 'moq.ts';
import {
  type FindOptionsWhere,
  type Repository,
  SelectQueryBuilder,
} from 'typeorm';

import { TodoService } from '~todo/services/todo.service';
import type { CreateTodo } from '~todo/dto/create.todo.dto';
import { Todo } from '~/todo/entities/todo.entity';

describe('TodoService', () => {
  let service: TodoService;
  const todo = Todo.fromPartial({
    id: 'a6119594-5067-4336-afbf-7efb2a38aac0',
    title: 'Make a sandwich',
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TodoService],
    })
      .useMocker((token) => {
        if (token === getRepositoryToken(Todo)) {
          const mock = new Mock<Repository<Todo>>()
            .setup((repository) => repository.create(It.IsAny()))
            // @ts-expect-error Mock create
            .callback(({ args: [dto] }) => Todo.fromCreateTodo(dto))
            .setup((repository) =>
              repository.save(It.Is((entity) => entity instanceof Todo)),
            )
            .callback(({ args: [entity] }) =>
              Promise.resolve(
                Todo.fromPartial({ completed: false, order: 0, ...entity }),
              ),
            )
            .setup((repository) => repository.createQueryBuilder('todo'))
            .returns(
              new Mock<SelectQueryBuilder<Todo>>({
                target: Object.create(SelectQueryBuilder),
              })
                .setup((query) => query.getMany())
                .returnsAsync([])
                .object(),
            )
            .setup((repository) =>
              repository.findOne(
                It.Is<Record<'where', FindOptionsWhere<Todo>>>(
                  (options) => options.where.id === todo.id,
                ),
              ),
            )
            .returnsAsync(todo)
            .setup((repository) =>
              repository.merge(
                It.Is((entity) => entity instanceof Todo),
                It.IsAny(),
              ),
            )
            .callback(({ args: [entity, dto] }) =>
              Todo.mergeUpdateTodo(entity, dto),
            )
            .setup((repository) =>
              repository.remove(It.Is<Todo>((entity) => entity.id === todo.id)),
            )
            // @ts-expect-error Mock remove result
            .returnsAsync(todo)
            .setup((repository) => repository.delete({}))
            .returnsAsync({ raw: null, affected: 10 });

          return mock.object();
        }
      })
      .compile();

    service = module.get<TodoService>(TodoService);
  });

  it('should be an instanceof TodoService', () => {
    expect(service).toBeInstanceOf(TodoService);
  });

  it('should create a new todo', async () => {
    const newTodo: CreateTodo = {
      title: 'Duis aute irure dolor in reprehenderit in voluptate velit esse',
    };

    const todo = await service.createOne(newTodo);

    expect(todo).toMatchObject<Todo>({
      id: expect.any(String),
      title: newTodo.title,
      completed: false,
      order: 0,
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
    });
  });

  it('should list all todos', async () => {
    const todos = await service.findAll();

    expect(Array.isArray(todos)).toBe(true);
  });

  it('should get one todo by id', async () => {
    await expect(
      service.findOne('a6119594-5067-4336-afbf-7efb2a38aac0'),
    ).resolves.toStrictEqual(todo);
  });

  it('should fail to get one todo if not exist', async () => {
    await expect(() => service.findOne(randomUUID())).rejects.toThrow(
      NotFoundException,
    );
  });

  it.each([{ completed: true }, { title: 'Lorem ipsum dolor sit amet' }])(
    'should update one todo by id',
    async (updates) => {
      await expect(
        service.updateOne(todo, updates),
      ).resolves.toMatchObject<Todo>({
        ...todo,
        ...updates,
        updatedAt: expect.any(Date),
      });
    },
  );

  it('should remove one todo by id', async () => {
    await expect(service.removeOne(todo)).resolves.toBeInstanceOf(Todo);
  });

  it('should remove all todos', async () => {
    await expect(service.removeAll()).resolves.toEqual([]);
  });
});
