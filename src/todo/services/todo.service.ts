import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import type { Repository } from 'typeorm';
import {
  applyQueryFields,
  applyQueryFilters,
  applyQueryPagination,
  applyQuerySort,
} from 'typeorm-extension';

import type { CreateTodo } from '~todo/dto/create.todo.dto';
import type { QueryTodo } from '~todo/dto/query-todo.dto';
import type { UpdateTodo } from '~todo/dto/update-todo.dto';
import { Todo } from '~todo/entities/todo.entity';

@Injectable()
export class TodoService {
  constructor(
    @InjectRepository(Todo)
    private readonly todoRepository: Repository<Todo>,
  ) {}

  createOne(newTodo: CreateTodo): Promise<Todo> {
    const todo = this.todoRepository.create(newTodo);

    return this.todoRepository.save(todo);
  }

  findAll({ fields, filter, page, sort }: QueryTodo = {}): Promise<
    Array<Todo>
  > {
    const alias = 'todo';
    const query = this.todoRepository.createQueryBuilder(alias);

    applyQuerySort(query, sort, {
      defaultAlias: alias,
      allowed: ['id', 'createdAt', 'title', 'updatedAt'],
    });

    applyQueryFields(query, fields, {
      defaultAlias: alias,
      allowed: ['id', 'completed', 'title'],
    });

    applyQueryFilters(query, filter, {
      defaultAlias: alias,
      allowed: ['completed', 'createdAt', 'updatedAt'],
    });

    applyQueryPagination(query, page);

    return query.getMany();
  }

  async findOne(id: Todo['id']): Promise<Todo> {
    const todo = await this.todoRepository.findOne({ where: { id } });

    if (!todo) {
      throw new NotFoundException(`Not found any todo with id: ${id}`);
    }

    return todo;
  }

  updateOne(todo: Todo, updatesTodo: UpdateTodo): Promise<Todo> {
    this.todoRepository.merge(todo, updatesTodo);

    return this.todoRepository.save(todo);
  }

  removeOne(todo: Todo): Promise<Todo> {
    return this.todoRepository.remove(todo);
  }

  async removeAll(): Promise<[]> {
    await this.todoRepository.delete({});
    return [];
  }
}
