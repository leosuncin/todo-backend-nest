import { faker } from '@faker-js/faker';
import { HttpStatus, type INestApplication } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Test } from '@nestjs/testing';
import { getDataSourceToken, TypeOrmModule } from '@nestjs/typeorm';
import jestOpenApi from 'jest-openapi';
import * as request from 'supertest';
import type { DataSource } from 'typeorm';
import { runSeeders, setDataSource } from 'typeorm-extension';

import { loadDataSourceOptions } from '~/data-source';
import { TodoModule } from '~todo/todo.module';

jestOpenApi(process.cwd() + '/openapi.yaml');

describe('TodoController (integration)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRootAsync({
          imports: [ConfigModule.forFeature(loadDataSourceOptions)],
          inject: [ConfigService],
          useFactory(config: ConfigService) {
            return config.getOrThrow('data-source');
          },
        }),
        TodoModule,
      ],
    }).compile();

    app = module.createNestApplication();
    const dataSource = module.get<DataSource>(getDataSourceToken());

    setDataSource(dataSource);
    await runSeeders(dataSource);
    await app.init();
  });

  afterEach(async () => {
    const dataSource = app.get<DataSource>(getDataSourceToken());

    await dataSource.dropDatabase();
    await app.close();
  });

  it('should create a new todo by sending a POST request', async () => {
    const data = { title: faker.lorem.sentence() };
    const response = await request(app.getHttpServer())
      .post('/todos')
      .send(data)
      .expect(HttpStatus.OK);

    expect(response).toSatisfyApiSpec();
    expect(response.body).toMatchObject({
      id: expect.any(String),
      title: data.title,
      completed: false,
      order: 0,
      url: expect.stringMatching(
        /\/todos\/[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}$/,
      ),
    });
  });

  it('should list all the todos by sending a GET request', async () => {
    const response = await request(app.getHttpServer())
      .get('/todos')
      .expect(HttpStatus.OK);

    expect(response).toSatisfyApiSpec();
    expect(Array.isArray(response.body)).toBe(true);
    expect(response.body).toHaveLength(10);
  });

  it('should remove all todos by sending a DELETE request', async () => {
    const response = await request(app.getHttpServer())
      .delete('/todos')
      .expect(HttpStatus.OK);

    expect(response).toSatisfyApiSpec();
    expect(response.body).toEqual([]);

    await request(app.getHttpServer())
      .get('/todos')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body).toHaveLength(0);
      });
  });

  it('should find one todo by sending a GET request with its ID', async () => {
    const response = await request(app.getHttpServer())
      .get('/todos/a6119594-5067-4336-afbf-7efb2a38aac0')
      .expect(HttpStatus.OK);

    expect(response).toSatisfyApiSpec();
    expect(response.body).toMatchObject({
      id: 'a6119594-5067-4336-afbf-7efb2a38aac0',
      title: 'Make a sandwich',
      completed: false,
      order: expect.any(Number),
      url: expect.stringContaining(
        '/todos/a6119594-5067-4336-afbf-7efb2a38aac0',
      ),
    });
  });

  it.each`
    property       | value
    ${'title'}     | ${'sudo make sandwich'}
    ${'completed'} | ${true}
  `(
    'should update $property to $value of a todo by sending a PATCH request',
    async ({ property, value }) => {
      const response = await request(app.getHttpServer())
        .patch('/todos/a6119594-5067-4336-afbf-7efb2a38aac0')
        .send({ [property]: value })
        .expect(HttpStatus.OK);

      expect(response).toSatisfyApiSpec();
      expect(response.body).toHaveProperty(
        'id',
        'a6119594-5067-4336-afbf-7efb2a38aac0',
      );
      expect(response.body).toHaveProperty(property, value);
    },
  );

  it('should remove one todo by sending a DELETE request with its ID', async () => {
    const response = await request(app.getHttpServer())
      .delete('/todos/a6119594-5067-4336-afbf-7efb2a38aac0')
      .expect(HttpStatus.OK);

    expect(response).toSatisfyApiSpec();
    await request(app.getHttpServer())
      .get('/todos')
      .expect(HttpStatus.OK)
      .expect(({ body }) => {
        expect(body).toHaveLength(9);
      });
  });

  it.each(['get', 'patch', 'delete'] as const)(
    'should fail when the todo not exist',
    async (method) => {
      const id = faker.datatype.uuid();
      const error = {
        error: 'Not Found',
        message: `Not found any todo with id: ${id}`,
        statusCode: 404,
      };

      const response = await request(app.getHttpServer())
        [method](`/todos/${id}`)
        .expect(HttpStatus.NOT_FOUND);

      expect(response).toSatisfyApiSpec();
      expect(response.body).toMatchObject(error);
    },
  );

  it.each([{}, { order: -1 }])(
    'should fail with validation errors when create a new todo %j',
    async (data) => {
      const response = await request(app.getHttpServer())
        .post('/todos')
        .send(data)
        .expect(HttpStatus.BAD_REQUEST);

      expect(response).toSatisfyApiSpec();
    },
  );
});
