import type { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { registerAs } from '@nestjs/config';
import { type DataSourceOptions, DataSource } from 'typeorm';
import type { SeederOptions } from 'typeorm-extension';

import { Todo } from '~todo/entities/todo.entity';
import { todoFactory } from '~todo/factories/todo.factory';
import { TodoSeeder } from '~todo/seeders/todo.seeder';

const options: DataSourceOptions & SeederOptions = {
  type: 'sqlite',
  database: process.env.NODE_ENV === 'test' ? ':memory:' : 'todo.db',
  entities: [Todo],
  migrations: [],
  factories: [todoFactory],
  seeds: [TodoSeeder],
};

export const loadDataSourceOptions = registerAs(
  'data-source',
  (): TypeOrmModuleOptions => ({
    ...options,
    synchronize: process.env.NODE_ENV === 'test',
    autoLoadEntities: true,
  }),
);

export const appDataSource = new DataSource(options);
