import { NestApplicationOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';

import { AppModule } from '~/app.module';

async function bootstrap(options?: NestApplicationOptions) {
  const app = await NestFactory.create(AppModule, options);
  const config = app.get(ConfigService);

  app.enableCors();
  await app.listen(config.get('PORT') ?? 3000);

  return app;
}

void bootstrap();
