import { HttpStatus, type INestApplication } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import * as pactum from 'pactum';

import { AppModule } from '~/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = module.createNestApplication();
    await app.init();
    await app.listen(0, '127.0.0.1');
    pactum.request.setBaseUrl(await app.getUrl());
  });

  afterAll(async () => {
    await app.close();
  });

  it('/ (GET)', async () => {
    await pactum
      .spec()
      .get('/')
      .expectStatus(HttpStatus.OK)
      .expectBody('Hello World!')
      .toss();
  });
});
