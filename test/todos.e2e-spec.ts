import { faker } from '@faker-js/faker';
import * as pactum from 'pactum';

import { createTodoFactory } from '~todo/factories/create-todo.factory';
import { updateTodoFactory } from '~todo/factories/update-todo.factory';

describe('TodoController (e2e)', () => {
  describe('the pre-requisites', () => {
    test('the api root responds to a GET (i.e. the server is up and accessible, CORS headers are set up)', async () => {
      await pactum
        .spec()
        .get('/todos')
        .expectHeader('access-control-allow-origin', '*')
        .toss();
    });

    test('the api root responds to a POST with the todo which was posted to it', async () => {
      const body = createTodoFactory.buildOne();

      await pactum
        .spec()
        .post('/todos')
        .withBody(body)
        .expectJsonLike({ title: body.title })
        .toss();
    });

    test('the api root responds successfully to a DELETE', async () => {
      await pactum.spec().delete('/todos').toss();
    });

    test('after a DELETE the api root responds to a GET with a JSON representation of an empty array', async () => {
      await pactum.spec().delete('/todos').toss();

      await pactum.spec().get('/todos').expectJson([]).toss();
    });
  });

  describe('storing new todos by posting to the root url', () => {
    const testCase = pactum.e2e('new todo');

    afterEach(async () => {
      await testCase.cleanup();
    });

    test('adds a new todo to the list of todos at the root url', async () => {
      const body = createTodoFactory.buildOne();

      await testCase
        .step('create')
        .spec()
        .post('/todos')
        .withBody(body)
        .stores('todo', '.')
        .clean()
        .delete('/todos/{id}')
        .withPathParams('id', '$S{todo.id}')
        .toss();

      await testCase
        .step('list')
        .spec()
        .get('/todos')
        .expect(({ res }) => {
          expect(res.body).toHaveLength(1);
          expect(res.body).toEqual([expect.objectContaining(body)]);
        })
        .toss();
    });

    test('sets up a new todo as initially not completed', async () => {
      await testCase
        .step('create')
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .stores('todo', '.')
        .clean()
        .delete('/todos/{id}')
        .withPathParams('id', '$S{todo.id}')
        .toss();

      await testCase
        .step('list')
        .spec()
        .get('/todos')
        .expect(({ res }) => {
          expect(res.body).toHaveLength(1);
          expect(res.body).toEqual([
            expect.objectContaining({ completed: false }),
          ]);
        })
        .toss();
    });

    test('each new todo has a url', async () => {
      await testCase
        .step('create')
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .stores('todo', '.')
        .clean()
        .delete('/todos/{id}')
        .withPathParams('id', '$S{todo.id}')
        .toss();

      await testCase
        .step('list')
        .spec()
        .get('/todos')
        .expect(({ res }) => {
          expect(res.body).toHaveLength(1);
          expect(res.body).toEqual([
            expect.objectContaining({ url: expect.any(String) }),
          ]);
        })
        .toss();
    });

    test('each new todo has a url, which returns a todo', async () => {
      const body = createTodoFactory.buildOne();

      const todo: { id: string; url: string } = await pactum
        .spec()
        .post('/todos')
        .withBody(body)
        .returns('.')
        .toss();

      await testCase
        .step('get')
        .spec()
        .get(todo.url)
        .expectJson(todo)
        .clean()
        .delete('/todos/{id}')
        .withPathParams('id', todo.id)
        .toss();
    });
  });

  describe('working with an existing todo', () => {
    afterEach(async () => {
      await pactum.spec().delete('/todos').toss();
    });

    test('can navigate from a list of todos to an individual todo via urls', async () => {
      await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .toss();
      await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .toss();

      const url: string = await pactum
        .spec()
        .get('/todos')
        .expect(({ res }) => {
          expect(res.body).toHaveLength(2);
        })
        .returns('[0].url')
        .toss();

      await pactum
        .spec()
        .get(url)
        .expectJsonLike({ title: 'typeof $V === "string"' });
    });

    test("can change the todo's title by PATCHing to the todo's url", async () => {
      const url: string = await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .returns('.url')
        .toss();
      const body = createTodoFactory.buildOne();

      await pactum.spec().patch(url).withBody(body).expectJsonLike(body);
    });

    test("can change the todo's completedness by PATCHing to the todo's url", async () => {
      const url: string = await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .returns('.url')
        .toss();
      const body = { completed: true };

      await pactum.spec().patch(url).withBody(body).expectJsonLike(body);
    });

    test('changes to a todo are persisted and show up when re-fetching the todo', async () => {
      const url: string = await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .returns('.url')
        .toss();
      const body = updateTodoFactory.buildOne({ completed: true });

      await pactum.spec().patch(url).withBody(body).expectJsonLike(body);
      await pactum.spec().get(url).expectJsonLike(body);
      await pactum
        .spec()
        .get('/todos')
        .expect(({ res }) => {
          expect(res.body).toHaveLength(1);
          expect(res.body).toEqual([expect.objectContaining(body)]);
        });
    });

    test("can delete a todo making a DELETE request to the todo's url", async () => {
      const url: string = await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne())
        .returns('.url')
        .toss();

      await pactum.spec().delete(url);
    });
  });

  describe('tracking todo order', () => {
    test('can create a todo with an order field', async () => {
      const body = createTodoFactory.buildOne({
        order: faker.datatype.number(),
      });

      await pactum
        .spec()
        .post('/todos')
        .withBody(body)
        .expectJsonLike(body)
        .toss();
    });

    test('can PATCH a todo to change its order', async () => {
      const url: string = await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne({ order: 523 }))
        .returns('.url')
        .toss();
      const body = {
        order: faker.datatype.number(),
      };

      await pactum.spec().patch(url).withBody(body).expectJsonLike(body);
    });

    test("remembers changes to a todo's order", async () => {
      const url: string = await pactum
        .spec()
        .post('/todos')
        .withBody(createTodoFactory.buildOne({ order: 10 }))
        .returns('.url')
        .toss();
      const body = {
        order: faker.datatype.number(),
      };

      await pactum.spec().patch(url).withBody(body);
      await pactum.spec().get(url).expectJsonLike(body);
    });
  });
});
